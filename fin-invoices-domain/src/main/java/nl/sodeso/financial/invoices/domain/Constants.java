package nl.sodeso.financial.invoices.domain;

/**
 * @author Ronald Mathies
 */
public class Constants {

    /**
     * Persistence unit name.
     */
    public final static String PU = "financialinvoices-pu";

}
