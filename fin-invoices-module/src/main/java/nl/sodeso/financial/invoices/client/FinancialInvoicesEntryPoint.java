package nl.sodeso.financial.invoices.client;

import com.google.gwt.core.client.GWT;
import nl.sodeso.financial.invoices.client.application.StartMenu;
import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;
import nl.sodeso.gwt.ui.client.link.LinkFactory;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.rpc.DefaultRunAsyncCallback;

/**
 * The application entry point is the main method of a GWT application, it should handle
 * the boodstrapping of the application.
 *
 * @author Ronald Mathies
 */
public class FinancialInvoicesEntryPoint extends ModuleEntryPoint {

    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }

    @Override
    public String getModuleName() {
        return "Invoices";
    }

    @Override
    public Icon getModuleIcon() {
        return Icon.Document;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initModule(final InitFinishedTrigger trigger) {
        GWT.runAsync(new DefaultRunAsyncCallback() {
            @Override
            public void success() {
                StartMenu.init();

                if (!LinkFactory.hasLink(getWelcomeToken())) {
                    LinkFactory.addLink(new WelcomeLink());
                }

                trigger.fire();
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getWelcomeToken() {
        return WelcomeLink.TOKEN;
    }

    /**
     * {@inheritDoc}
     */
    public void activateModule(final ActivateFinishedTrigger trigger) {
        GWT.runAsync(new DefaultRunAsyncCallback() {
            @Override
            public void success() {
                trigger.fire();
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void suspendModule(final SuspendFinishedTrigger trigger) {
        GWT.runAsync(new DefaultRunAsyncCallback() {
            @Override
            public void success() {
                trigger.fire();
            }
        });
    }
}
