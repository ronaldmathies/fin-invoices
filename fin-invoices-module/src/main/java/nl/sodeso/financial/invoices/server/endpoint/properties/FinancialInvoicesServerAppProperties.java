package nl.sodeso.financial.invoices.server.endpoint.properties;

import nl.sodeso.gwt.ui.server.util.ServerAppProperties;

/**
 * @author Ronald Mathies
 */
public class FinancialInvoicesServerAppProperties extends ServerAppProperties {

    public FinancialInvoicesServerAppProperties(String configurationFile) {
        super(configurationFile);
    }
}
