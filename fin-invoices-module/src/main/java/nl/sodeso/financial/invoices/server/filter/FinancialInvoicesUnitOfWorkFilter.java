package nl.sodeso.financial.invoices.server.filter;

import nl.sodeso.financial.invoices.domain.Constants;
import nl.sodeso.persistence.hibernate.filter.UnitOfWorkFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * @author Ronald Mathies
 */
@WebFilter(
    urlPatterns = {"/*"},
    initParams = {
        @WebInitParam(name = UnitOfWorkFilter.INIT_PERSISTENCE_UNIT, value = Constants.PU),
        @WebInitParam(name = UnitOfWorkFilter.INIT_EXTRA_URL_PATTERN, value = ".*/financialinvoices/.*")
    }
)
public class FinancialInvoicesUnitOfWorkFilter extends UnitOfWorkFilter {
}
