package nl.sodeso.financial.invoices.server.listener;

import nl.sodeso.financial.invoices.client.Constants;
import nl.sodeso.gwt.ui.server.listener.AbstractPropertiesInitializerContextListener;

import javax.servlet.annotation.WebListener;

/**
 * @author Ronald Mathies
 */
@WebListener
public class FinancialInvoicesPropertiesInitializerContextListener extends AbstractPropertiesInitializerContextListener {

    @Override
    public Class getServerAppPropertiesClass() {
        return FinancialInvoicesServerAppProperties.class;
    }

    @Override
    public String getConfiguration() {
        return "fin-invoices-configuration.properties";
    }

    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }
}
