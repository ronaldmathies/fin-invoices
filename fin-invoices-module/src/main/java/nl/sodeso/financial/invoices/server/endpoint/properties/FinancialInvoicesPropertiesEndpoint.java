package nl.sodeso.financial.invoices.server.endpoint.properties;

import nl.sodeso.financial.invoices.client.Constants;
import nl.sodeso.financial.invoices.client.properties.FinancialInvoicesClientAppProperties;
import nl.sodeso.financial.invoices.server.listener.FinancialInvoicesServerAppProperties;
import nl.sodeso.gwt.ui.server.endpoint.properties.AbstractPropertiesEndpoint;
import nl.sodeso.gwt.ui.server.util.ApplicationPropertiesContainer;

import javax.servlet.annotation.WebServlet;

/**
 * By default the application has an endpoint for properties consisting of the following settings:
 *
 * <ul>
 *  <li>name: Name of the application (used as the title)</li>
 *  <li>version: Version number of the application.</li>
 *  <li>devkitEnabled: Are the extra development tools available.</li>
 * </ul>
 *
 * If you would like to have more global application settings (not user specific), that don't
 * change during a session, then this is the place to add them. Just add the fields below
 * and fill them using the endpoint.
 *
 * @author Ronald Mathies
 */
@WebServlet(urlPatterns = {"*.financialinvoices-properties"})
public class FinancialInvoicesPropertiesEndpoint extends AbstractPropertiesEndpoint<FinancialInvoicesClientAppProperties, FinancialInvoicesServerAppProperties> {

    @Override
    public void fillApplicationProperties(FinancialInvoicesServerAppProperties serverAppProperties, FinancialInvoicesClientAppProperties clientAppProperties) {
    }

    @Override
    public Class getClientAppPropertiesClass() {
        return FinancialInvoicesClientAppProperties.class;
    }

    @Override
    public FinancialInvoicesServerAppProperties getServerAppProperties() {
        return ApplicationPropertiesContainer.instance().getApplicationProperties(Constants.MODULE_ID);
    }

}

